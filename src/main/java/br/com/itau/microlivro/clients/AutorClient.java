package br.com.itau.microlivro.clients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.itau.microlivro.viewobjects.Autor;

@FeignClient(name="autor")
public interface AutorClient {

	@GetMapping
	public List<Autor> buscarAutores();

	
}





